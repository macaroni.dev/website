* Template used: https://github.com/BulmaTemplates/bulma-templates/blob/1d74a5a95d1c28ac5b8aef0b6520212c464e41dd/templates/personal.html

## Content
### about
* 10+ years experience
* BigCo's, startups, technical cofounder

### engineering
* Go
* Haskell & FP
* Databases (Postgres)
* Nix
* Slack
* Cloud etc blah blah

### gamedev
* Aspiring
* Haskell Libraries

### contact
* hello@macaroni.dev